#!/usr/bin/env python
############################################
# OPC-UA client for the EMRB-opc-client
# Carlos.Solans@cern.ch
############################################

from opcua import Client
from opcua import ua

class emrb_opc_client:
    def __init__(self, connstr,connect=False):
        self.verbose=False
        self.nodes = {}
        self.connstr = connstr
        self.client = Client(connstr)
        self.client.session_timeout = 600000
        self.client.secure_channel_timeout = 600000
        if connect: self.Open()
        pass
    def Open(self):
        try:
            self.client.connect()
        except:
            print("Cannot connect to server on: %s" % self.connstr)
            return False
        self.FindNodes(self.client.get_objects_node().get_children(),self.nodes,["brd1","A_","channelNumber"])
        self.FindNodes(self.client.get_objects_node().get_children(),self.nodes,["brd1","A_","dateAndTime"])
        self.FindNodes(self.client.get_objects_node().get_children(),self.nodes,["brd1","A_","ADCCalibrationFactor"])
        self.FindNodes(self.client.get_objects_node().get_children(),self.nodes,["brd1","A_","enable"])
        self.FindNodes(self.client.get_objects_node().get_children(),self.nodes,["brd1","A_","ADCValue"])
        self.FindNodes(self.client.get_objects_node().get_children(),self.nodes,["brd1","A_","Voltage"])
        self.FindNodes(self.client.get_objects_node().get_children(),self.nodes,["brd1","A_","Temperature"])
        self.FindNodes(self.client.get_objects_node().get_children(),self.nodes,["brd1","B_","channelNumber"])
        self.FindNodes(self.client.get_objects_node().get_children(),self.nodes,["brd1","B_","dateAndTime"])
        self.FindNodes(self.client.get_objects_node().get_children(),self.nodes,["brd1","B_","ADCCalibrationFactor"])
        self.FindNodes(self.client.get_objects_node().get_children(),self.nodes,["brd1","B_","enable"])
        self.FindNodes(self.client.get_objects_node().get_children(),self.nodes,["brd1","B_","ADCValue"])
        self.FindNodes(self.client.get_objects_node().get_children(),self.nodes,["brd1","B_","Voltage"])
        self.FindNodes(self.client.get_objects_node().get_children(),self.nodes,["brd1","B_","Temperature"])
        return True
    def FindNodes(self,objs,nodes,keys):
        for obj in objs:
            if self.verbose: print("Parsing %s" % obj.get_display_name().Text)
            if "transportMech" in obj.get_display_name().Text: continue
            if keys[0] in obj.get_display_name().Text:
                if self.verbose: print("Found node: %s" % (obj.get_display_name().Text)) 
                if len(keys)>1:
                    if not obj.get_display_name().Text in nodes: nodes[obj.get_display_name().Text]={}
                    self.FindNodes(obj.get_children(),nodes[obj.get_display_name().Text],keys[1:])
                    pass
                else:
                    if self.verbose: print("Found node: %s" % obj.get_display_name().Text)
                    nodes[obj.get_display_name().Text]=obj
                    pass
                pass
            pass
        return nodes
    def Close(self):
        self.client.disconnect()
        pass
    def GetNodes(self):
        return self.nodes
    def PrintServerInfo(self):
        self.root = self.client.get_root_node()
        print("Root node is: ", self.client.get_root_node())
        print("Children of root are: ", self.client.get_root_node().get_children())
        print("Children of objects are: ", self.client.get_objects_node().get_children())
        pass
    def SetVerbose(self, v):    
        self.verbose = v
        pass
    def GetNode(self,name,attr):
        return self.nodes["brd1"][name][attr]
    def GetChans(self):
        return self.nodes["brd1"].keys()
    def GetChannelNumber(self,chan):
        return self.nodes["brd1"][chan]["channelNumber"].get_value()
    pass

if __name__=="__main__":
    
    import os
    import sys
    import signal
    import argparse
    import time
    import datetime

    constr="opc.tcp://pcatlidrpi04:4841"
    
    parser=argparse.ArgumentParser()
    parser.add_argument('-s','--constr',help="connection string: %s" % constr, default=constr)
    parser.add_argument("-d","--delay",help="Delay time [s] between measurements.Default 0",type=int,default=2)
    parser.add_argument('-v','--verbose',help="enable verbose mode", action="store_true")
    
    args=parser.parse_args()
    
    client=emrb_opc_client(args.constr)
    client.SetVerbose(args.verbose)
    if not client.Open(): sys.exit()
    if args.verbose: print(client.GetNodes())
    
    cont = True
    def signal_handler(signal, frame):
        print("You pressed ctrl+C")
        global cont
        cont = False
        return

    signal.signal(signal.SIGINT, signal_handler)

    print("Reading")
    
    while True:
        for chan in client.GetChans():
            if cont==False: break
            try:
                s ="Chan: %s, " % chan
                s+="channelNumber: %s, " % client.GetNode(chan,"channelNumber").get_value()
                s+="dateAndTime: %s, " % client.GetNode(chan,"dateAndTime").get_value()
                s+="ADCCalibrationFactor: %s, " % client.GetNode(chan,"ADCCalibrationFactor").get_value()
                s+="enable: %s, " % client.GetNode(chan,"enable").get_value()
                s+="ADCValue: %s, " % client.GetNode(chan,"ADCValue").get_value()
                s+="Voltage: %s, " % client.GetNode(chan,"Voltage").get_value()
                s+="Temperature: %s, " % client.GetNode(chan,"Temperature").get_value()
                print(s)
            except ua.uaerrors.UaStatusCodeError:
                print ("Cannot read something from channel: %s" % chan)
                pass
            pass
        if cont==False: break
        time.sleep(args.delay)
        pass
    pass
    print("Closing connection")
    client.Close()
    print("Have a nice day")
    pass
